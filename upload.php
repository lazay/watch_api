<!doctype html>
<html>
<head>
  <meta charset="utf-8">
   <title>POPWatch</title>
   <link rel="stylesheet" type="text/css" href="style.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
   integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>

<body>
<div class="row">

<?php
include("config.php");
session_start();
// UPLOADS FILES
    $dir = 'uploads/';
    $name = $dir . basename($_FILES['userfile']['name']);
    $size = filesize($_FILES['userfile']['tmp_name']);
    $sizemax = 524288000;
    $extension = strrchr($_FILES['userfile']['name'], '.');

//On fait un tableau contenant les extensions autorisées.
    $extensions = array('.png','.jpg','.jpeg','.pdf','.ppt','.pps','.ppsx');
//Si l'extension n'est pas dans le tableau
    if(!in_array($extension, $extensions))
      {
      $error = "<h3 class='col-md-4 col-md-offset-4'>Sorry this type of file isn't allowed</h3>
      <a href='post.php' title=''>
        <button type='button' class='btn btn-primary col-md-2 col-md-offset-5'>
          Try again
        </button>
      </a>";
      }

//Check taille du fichier
      if($size>$sizemax)
      {
           $error = "<h3 class='col-md-4 col-md-offset-4'>your file size must be 500Mo maximum</h3>
           <a href='post.php' title=''>
             <button type='button' class='btn btn-primary col-md-2 col-md-offset-5'>
               Try again
             </button>
           </a>";
      }

if(!isset($error)) //S'il n'y a pas d'erreur, on upload
    {
         //On formate le nom du fichier ici...
         $name = strtr($name,
              'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
              'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
         $name = preg_replace('/([^.a-z0-9]+)/i', '-', $name);
         if(move_uploaded_file($_FILES['userfile']['tmp_name'], $dir . $name))
          //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
         {
              echo "<h3 class='col-md-4 col-md-offset-4'>Upload succeed, your watch has been posted</h3>
              <a href='welcome.php' title=''>
                <button type='button' class='btn btn-primary col-md-2 col-md-offset-5'>
                  Home
                </button>
              </a>";
         }
         else //Sinon (la fonction renvoie FALSE).
         {
              echo "<h3 class='col-md-4 col-md-offset-4'>Upload failed</h3>
              <a href='post.php' title=''>
                <button type='button' class='btn btn-primary col-md-2 col-md-offset-5'>
                  Try again
                </button>
              </a>";
         }
    }
else
    {
         echo $error;
    }

//Recupere le plus grand nombre dans id de veille la derniere veille etant
//celle que l'user vient d'ajouter
$req = "SELECT MAX(id) FROM watch";
$result = mysqli_query($handle,$req);
$idwatch=mysqli_fetch_assoc($result);
$countId = $idwatch['MAX(id)'];


// Insertion content
$updateupload = "UPDATE content SET name='$name' WHERE idwatch = '$countId'";
$insert = mysqli_query($handle,$updateupload);
?>
</div>
</body>
</html>
