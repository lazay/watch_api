- You need to have a server running php and phpmyadmin
- Put the project directory on your server in /var/www/html
- Open phpmyadmin in your browser, create a new empty database, name it api_veille
 and import api_veille.sql in it
- Now open config.php in your text editor and modify the information to
 match your server and database :
 $handle = mysqli_connect("localhost","phpmyadmin_username","phpmyadmin_password",
 "api_veille");
- You can now access the project through your server in your web browser typing :
Server_IP/watch_api
- You can now sign in to post watches
