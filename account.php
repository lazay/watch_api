<!doctype html>
<html>
<head>
  <meta charset="utf-8">
   <title>POPWatch</title>
   <link rel="stylesheet" type="text/css" href="style.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
   integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>

<body>
<div class="row">
<?php
include("config.php");

$name = $_GET["name"];
$firstname = $_GET["firstname"];
$username = $_GET["username"];
$mail = $_GET["mail"];
//$promo = $_GET["promo"];
$pass = $_GET["password"];
$cfpass = $_GET["cfpassword"];

// Check username pas déjà pris
$usercheck = "SELECT username FROM users WHERE username ='$username'";
$result = mysqli_query($handle,$usercheck);
$row_cnt = $result->num_rows;
if ($row_cnt!=0) {
  echo "<h3 class='col-md-6 col-md-offset-4'>Username already exists, choose a different one</h3>";
}
// Check mdp bien tapé et confirmé
elseif ($pass!=$cfpass) {
  echo "<h3 class='col-md-4 col-md-offset-4'>Error while submitting password, please try again</h3>";
}

else {
  // Hachage du mot de passe
  $pass_hache = sha1($pass);

  // Insertion
  $adduser = "INSERT INTO users(name, firstname, mail, username, password)
  VALUES('$name', '$firstname', '$mail', '$username', '$pass_hache')";
  $add = mysqli_query($handle,$adduser);

  include("login.php");


}
?>
<a href='sign.php' title=''>
  <button type="button" class="btn btn-primary col-md-2 col-md-offset-5">
    Try again
  </button>
</a>
</div>
</body>
</html>
