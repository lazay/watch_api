<!doctype html>
<html>
<head>
  <meta charset="utf-8">
   <title>POPWatch</title>
   <link rel="stylesheet" type="text/css" href="style.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
   integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>

<body>
  <?php
  include("config.php");
  ?>

<div class='container-fluid'>
  <div class="row">
    <nav class="navbar navbar-inverse navbar-static-top" id='nav'>
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed menu-btn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class='nav navbar-nav col-md-12'>
            <li class="col-md-2 col-md-offset-1">
              <a href='index.php' title='' class="text-uppercase text-center">home</a>
            </li>
            <li class="dropdown col-md-2">
              <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
              role="button" aria-haspopup="true" aria-expanded="false">watchs<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="text-uppercase text-center"><a href="categoryindex.php?idsubject=1">Web</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="categoryindex.php?idsubject=2">Sofware</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="categoryindex.php?idsubject=3">Hardware</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="categoryindex.php?idsubject=4">Mobile</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="categoryindex.php?idsubject=5">Developpement</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="categoryindex.php?idsubject=6">creative</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="categoryindex.php?idsubject=7">Gaming</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="categoryindex.php?idsubject=8">Other</a></li>
              </ul>
            </a>
          </li>
          <li class="col-md-2">
            <a href='' title='' class="text-uppercase text-center" data-toggle="modal" data-target="#random">random</a>
          </li>
          <li class="dropdown col-md-2">
            <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
            role="button" aria-haspopup="true" aria-expanded="false">users<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="text-uppercase text-center"><a href="#">alan turing</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="#">ada lovelace</a></li>
            </ul>
          </a>
        </li>
        <li class="col-md-2">
          <a href='' title='' class="text-uppercase text-center" data-toggle="modal" data-target="#account">log in</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>

<div class="modal fade" id="account" tabindex="-1" role="dialog" aria-labelledby="Connexion">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="Connexion">Connexion</h4>
      </div>
      <div class="modal-body">
        <form action="login.php" method="get">
          <input name='username' type="text" placeholder="Username">
          <label for='username'></label>
          <input name='password' type="password" placeholder="Password">
          <label for='password'></label>
      </div>
      <div class="modal-footer">
        <input class="btn btn-primary" type="submit" value='Log in'>
        <h4 class="text-center"> Don't have an account yet ? </h4>
        <a href='sign.php' title=''><button type="button" class="btn btn-default sign">Sign in</button></a>
      </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="random" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Spin the watch wheel</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="randomindex.php">
          <label for="rand1" class="col-md-2 col-md-offset-1">
            <input type="checkbox" id="rand1" value="Amélie" checked name="name[]"> Amélie
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand2" class="col-md-2">
            <input type="checkbox" id="rand2" value="Aurélie" checked name="name[]"> Aurélie
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand3" class="col-md-2">
            <input type="checkbox" id="rand3" value="Bastien" checked name="name[]"> Bastien
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand4" class="col-md-2">
            <input type="checkbox" id="rand4" value="Christopher" checked name="name[]"> Christopher
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand5" class="col-md-2">
            <input type="checkbox" id="rand5" value="Camille" checked name="name[]"> Camille
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand6" class="col-md-2 col-md-offset-1">
            <input type="checkbox" id="rand6" value="Jérémy" checked name="name[]"> Jérémy
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand7" class="col-md-2">
            <input type="checkbox" id="rand7" value="Ingrid" checked name="name[]"> Ingrid
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand8" class="col-md-2">
            <input type="checkbox" id="rand8" value="Selim" checked name="name[]"> Selim
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand9" class="col-md-2">
            <input type="checkbox" id="rand9" value="Loïs" checked name="name[]"> Loïs
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand10" class="col-md-2">
            <input type="checkbox" id="rand10" value="Jean-Pierre" checked name="name[]"> Jean-pierre
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand11" class="col-md-2 col-md-offset-1">
            <input type="checkbox" id="rand11" value="Rémy" checked name="name[]"> Rémy
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand12" class="col-md-2">
            <input type="checkbox" id="rand12" value="Jimmylan" checked name="name[]"> Jimmylan
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand13" class="col-md-2">
            <input type="checkbox" id="rand13" value="Kilian" checked name="name[]"> Kilian
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand14" class="col-md-2">
            <input type="checkbox" id="rand14" value="Léo" checked name="name[]"> Léo
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand15" class="col-md-2">
            <input type="checkbox" id="rand15" value="Marie" checked name="name[]"> Marielle
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand16" class="col-md-2 col-md-offset-1">
            <input type="checkbox" id="rand16" value="Maxime" checked name="name[]"> Maxime
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand17" class="col-md-2">
            <input type="checkbox" id="rand17" value="Maxence" checked name="name[]"> Maxence
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand18" class="col-md-2">
            <input type="checkbox" id="rand18" value="Pierre" checked name="name[]"> Pierre
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand19" class="col-md-2">
            <input type="checkbox" id="rand19" value="Mathilde" checked name="name[]"> Mathilde
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand20" class="col-md-2">
            <input type="checkbox" id="rand20" value="Théo" checked name="name[]"> Théo
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand21" class="col-md-2 col-md-offset-4">
            <input name='lol' type="checkbox" id="rand21" value="Victor" checked name="name[]"> Victor
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <label for="rand22" class="col-md-2">
            <input type="checkbox" id="rand22" value="Younes" checked name="name[]"> Younes
            <img src="chatmarrant.jpg" class="img-responsive" alt="">
          </label>
          <input type="submit" id="bottom-button">
        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<?php
$query="SELECT * FROM watch";
$result=mysqli_query($handle,$query);
echo "<ul class='watch col-md-offset-3 text-center col-md-6'>";
include "homee.php";
while($test=mysqli_fetch_array($result)){
  $line=mysqli_fetch_array($join);
  $name=mysqli_fetch_array($user);
  echo "<li class='watchhead'>";
  echo "<div class='titlew row'>" . "<h3 class='watchtt col-md-12 text-uppercase'>" . $test["title"] . "</h2>" . "</div>";
  echo "<div class='watchvalue'>" . $line["value"] . "<br/>" . "</div>";
  echo "<div class='row'>" . "<span class='text-uppercase col-md-4 col-xs-12'>" . $name["name"] . "&nbsp" . $name["firstname"] . "</span>" . "<a class='col-md-4 col-xs-12' href=" . $line['source'] . ">" . "SOURCE" . "</a>" . "<span class='col-md-4 col-xs-12'>" . $test["ddate"] . "</span>";
  echo "</li>";
}
echo "</ul>";
?>

</div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
